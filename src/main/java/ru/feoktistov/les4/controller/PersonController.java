package ru.feoktistov.les4.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.feoktistov.les4.entity.Person;
import ru.feoktistov.les4.repository.PersonRepository;

import javax.jws.WebParam;
import java.util.List;

@Controller
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @GetMapping("/addPerson")
    public String main(Person person, ModelAndView model) {
        model.addObject("person",person );
        return "main";
    }

    @PostMapping("/addPerson")
    public String  newEmployee(@ModelAttribute Person person, Model model) {
        model.addAttribute("person", person);
        personRepository.save(person);
        return "redirect:/allperson";
    }

    @GetMapping("/allperson")
    public String allPerson(Person person, Model model){
        Iterable<Person> personList = personRepository.findAll();
        model.addAttribute("persons", personList);
        return "allperson";
    }
}
