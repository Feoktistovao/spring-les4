package ru.feoktistov.les4.entity;


import javax.persistence.*;
import java.util.List;


@Entity
public class Person  {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String userName;

    private String firstName;

    private String lastName;

    private String password;

    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    private List<Task> tasks;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "id: " + this.id + " FistName: " + this.firstName + " LastName: " + this.lastName + "  ListFriends " + this.tasks ;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



}
