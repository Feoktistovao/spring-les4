package ru.feoktistov.les4.repository;

import org.springframework.data.repository.CrudRepository;
import ru.feoktistov.les4.entity.Person;

import java.util.List;


public interface PersonRepository extends CrudRepository<Person, Long> {

}
