package ru.feoktistov.les4.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import ru.feoktistov.les4.entity.Person;
import ru.feoktistov.les4.entity.Task;

import java.util.List;
import java.util.SortedMap;

public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findByPerson(Person person, Sort sort);
}
